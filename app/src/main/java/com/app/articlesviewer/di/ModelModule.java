package com.app.articlesviewer.di;

import com.app.articlesviewer.net.ApiInterface;
import com.app.articlesviewer.net.ApiModule;
import com.app.articlesviewer.other.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yulya on 27.05.2017.
 */
@Module
public class ModelModule {
    @Provides
    @Singleton
    ApiInterface provideApiInterface() {
        return ApiModule.getApiInterface(Constants.BASE_URL);
    }
}
