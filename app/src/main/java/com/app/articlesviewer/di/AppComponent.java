package com.app.articlesviewer.di;

import com.app.articlesviewer.adapters.ArticleAdapter;
import com.app.articlesviewer.adapters.SearchResultAdapter;
import com.app.articlesviewer.presenter.ArticlesPresenter;
import com.app.articlesviewer.presenter.SearchPresenter;
import com.app.articlesviewer.repository.RemoteDataStore;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Yulya on 27.05.2017.
 */
@Singleton
@Component(modules = {AppModule.class, ModelModule.class, PresenterModule.class})
public interface AppComponent {
    void inject(RemoteDataStore dataStore);

    void inject(ArticlesPresenter presenter);

    void inject(SearchPresenter presenter);

    void inject(ArticleAdapter adapter);

    void inject(SearchResultAdapter adapter);
}
