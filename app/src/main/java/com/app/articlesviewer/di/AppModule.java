package com.app.articlesviewer.di;

import android.app.Application;
import android.content.Context;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yulya on 27.05.2017.
 */
@Module
public class AppModule {
    private Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Context provideApplication() {
        return this.mApplication;
    }

    @Provides
    @Singleton
    Picasso providePicasso() {
        return Picasso.with(mApplication.getApplicationContext());
    }
}
