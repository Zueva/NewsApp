package com.app.articlesviewer.repository;

import com.app.articlesviewer.ArticlesViewerApplication;
import com.app.articlesviewer.model.Article;
import com.app.articlesviewer.net.ApiInterface;
import com.app.articlesviewer.other.Constants;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by Yulya on 27.05.2017.
 */

public class RemoteDataStore implements DataStore {

    @Inject
    protected ApiInterface mApiInterface;
    @Inject
    public RemoteDataStore() {
        ArticlesViewerApplication.getAppComponent().inject(this);
    }

    @Override
    public ArticleStorage getArticleStorage() {
        return new ArticleStorage() {
            @Override
            public Observable<List<Article>> getArticles(String section, int timePeriod, int offset) {
                return mApiInterface.getArticles(section, timePeriod, Constants.ARTICLES_MOST_POPULAR, offset)
                        .map(articles -> articles.getItems());
            }

            @Override
            public Observable<List<Article>> searchArticle(String query) {
                return mApiInterface.searchArticle(Constants.ARTICLES_SEARCH, query)
                        .map(articles -> articles.getArticles().getItems());
            }
        };
    }
}
