package com.app.articlesviewer.repository;

import com.app.articlesviewer.model.Article;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Yulya on 27.05.2017.
 */

public interface DataStore {

    ArticleStorage getArticleStorage();

    interface ArticleStorage {
        Observable<List<Article>> getArticles(String section, int timePeriod, int offset);

        Observable<List<Article>> searchArticle(String query);
    }
}
