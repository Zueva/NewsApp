package com.app.articlesviewer.repository;

import com.app.articlesviewer.model.Article;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by Yulya on 27.05.2017.
 */
@Singleton
public class ArticlesRepository implements DataStore.ArticleStorage {

    private final RemoteDataStore mRemoteDataStore;

    private final RemoteDataStore mLocalDataStore;

    List<Article> mCachedArticles;

    @Inject
    public ArticlesRepository(RemoteDataStore remoteDataStore, RemoteDataStore localDataStore) {
        this.mRemoteDataStore = remoteDataStore;
        this.mLocalDataStore = localDataStore;
    }

    @Override
    public Observable<List<Article>> getArticles(String section, int timePeriod, int offset) {
        return mRemoteDataStore.getArticleStorage().getArticles(section, timePeriod, offset);
    }

    @Override
    public Observable<List<Article>> searchArticle(String query) {
        return mRemoteDataStore.getArticleStorage().searchArticle(query);
    }

    public void saveArticles(List<Article> articles) {
        if (mCachedArticles == null) {
            mCachedArticles = new ArrayList<>();
        }
        mCachedArticles.addAll(articles);
    }

    public boolean cachedArticlesAvailable(){
        return mCachedArticles != null && !mCachedArticles.isEmpty();
    }

    public List<Article> getCachedArticles() {
        return mCachedArticles;
    }
}
