package com.app.articlesviewer.net;

import com.app.articlesviewer.model.Articles;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yulya on 27.05.2017.
 */

public class ApiModule {
    public static ApiInterface getApiInterface(String url){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);


        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Articles.class, new ArticlesDeserializer())
                .create();

        Retrofit.Builder builder = new Retrofit.Builder().
                baseUrl(url)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        ApiInterface apiInterface = builder.build().create(ApiInterface.class);
        return  apiInterface;
    }

}
