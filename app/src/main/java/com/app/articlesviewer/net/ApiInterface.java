package com.app.articlesviewer.net;

import com.app.articlesviewer.model.Articles;
import com.app.articlesviewer.model.SearchArticles;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Yulya on 27.05.2017.
 */

public interface ApiInterface {
    @GET("/svc/mostpopular/v2/mostviewed/{section}/{time-period}.json")
    Observable<Articles> getArticles(@Path("section") String section,
                                     @Path("time-period") int period,
                                     @Query("api-key") String key,
                                     @Query("offset") int offset);

    @GET("/svc/search/v2/articlesearch.json")
    Observable<SearchArticles> searchArticle(@Query("api-key") String key,
                                             @Query("q") String query);
}
