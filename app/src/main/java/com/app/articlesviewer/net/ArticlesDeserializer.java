package com.app.articlesviewer.net;

import com.app.articlesviewer.model.ArticleImpl;
import com.app.articlesviewer.model.Articles;
import com.app.articlesviewer.model.Headline;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yulya on 31.05.2017.
 */

public class ArticlesDeserializer implements JsonDeserializer<Articles> {

    @Override
    public Articles deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        JsonArray array = jsonObject.getAsJsonArray("results");
        if (array == null) {
            array = jsonObject.getAsJsonArray("docs");
        }
        return parseArticles(array);
    }

    private Articles parseArticles(JsonArray array) {
        Articles articles = new Articles();
        List<ArticleImpl> articlesItems = new ArrayList<>();
        for (JsonElement articleElement : array) {
            JsonObject articleJson = articleElement.getAsJsonObject();
            ArticleImpl article = new ArticleImpl();
            if (articleJson.get("title") != null && !articleJson.get("title").isJsonNull()) {
                article.setTitle(articleJson.get("title").getAsString());
            }

            if (articleJson.get("published_date") != null && !articleJson.get("published_date").isJsonNull()) {
                article.setDate(articleJson.get("published_date").getAsString());
            }
            if (articleJson.get("pub_date") != null && !articleJson.get("pub_date").isJsonNull()) {
                article.setDate(articleJson.get("pub_date").getAsString());
            }
            try {
            if (articleJson.get("abstract") != null && !articleJson.get("abstract").isJsonNull()) {
                article.setDescription(articleJson.get("abstract").getAsString());
            }}catch (Exception e) {
                e.getStackTrace();
            }

            JsonObject headlineJson = articleJson.getAsJsonObject("headline");
            if (headlineJson != null) {
                if(headlineJson.get("main") != null && !headlineJson.get("main").isJsonNull()) {
                    Headline headline = new Headline();
                    headline.setTitle(headlineJson.get("main").getAsString());
                    article.setHeadline(headline);
                }
            }
            parseMetaData(articleJson, "media", article);
            parseMetaData(articleJson, "multimedia", article);
            articlesItems.add(article);
        }

        articles.setArticles(articlesItems);
        return articles;
    }

    private void parseMetaData(JsonObject jsonObject, String tag ,ArticleImpl article) {
        String imgUrl = null;
        List<String> url = null;
        if (jsonObject.get(tag) instanceof JsonObject) {
            JsonObject mediaObj = jsonObject.getAsJsonObject(tag);
            if (mediaObj  != null) imgUrl = parseUrl(mediaObj, "url");
        }
        else if (jsonObject.get(tag) instanceof JsonArray){
            JsonArray media = jsonObject.getAsJsonArray(tag);
            if (media != null) {
                url = new ArrayList<>();
                for (JsonElement item : media) {
                    JsonObject mediaItem = item.getAsJsonObject();
                    JsonArray mediaData = mediaItem.getAsJsonArray("media-metadata");
                    if (mediaData != null) {
                        for (JsonElement mediaDataItem : mediaData) {
                            JsonObject image = mediaDataItem.getAsJsonObject();
                            if (image != null) {
                                String itemUrl = parseUrl(image, "url");
                                if (itemUrl != null) {
                                    url.add(itemUrl);
                                }
                            }
                        }
                    }

                    String mediaItemUrl = parseUrl(mediaItem, "url");
                    if (mediaItemUrl != null) {
                        url.add(mediaItemUrl);
                    }
                }
            }
        }
        if (url != null && !url.isEmpty()) {
            article.setMediaItems(url);
        } else if (imgUrl != null) {
            article.setMediaItems(imgUrl);
        }
    }

    private String parseUrl(JsonObject jsonObject, String tag) {
        if (jsonObject.get(tag) != null && !jsonObject.get(tag).isJsonNull()) {
            return jsonObject.get(tag).getAsString();
        }
        return null;
    }
}
