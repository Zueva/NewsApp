package com.app.articlesviewer;

import android.app.Application;

import com.app.articlesviewer.di.AppComponent;
import com.app.articlesviewer.di.AppModule;
import com.app.articlesviewer.di.DaggerAppComponent;

/**
 * Created by Yulya on 27.05.2017.
 */

public class ArticlesViewerApplication extends Application {
    private static AppComponent mAppComponent;
    public static AppComponent getAppComponent() {return mAppComponent;}
    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = buildAppComponent();
    }

    protected AppComponent buildAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}
