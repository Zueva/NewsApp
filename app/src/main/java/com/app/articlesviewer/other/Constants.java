package com.app.articlesviewer.other;

/**
 * Created by Yulya on 27.05.2017.
 */

public interface Constants {
    String BASE_URL = "https://api.nytimes.com/svc/";

    String UI_THREAD = "UI_THREAD";

    String IO_THREAD = "IO_THREAD";

    String ARTICLES_SECTION = "Arts";

    int ARTICLES_TIME_PERIOD = 7;

    String ARTICLES_MOST_POPULAR = "07b3f50402b34ad4bbef20aca9d74ac4";

    String ARTICLES_SEARCH = "07b3f50402b34ad4bbef20aca9d74ac4";

    int OFFSET_STEP = 20;
}
