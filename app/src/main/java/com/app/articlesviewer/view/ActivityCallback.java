package com.app.articlesviewer.view;

/**
 * Created by Julya on 29.05.2017.
 */

public interface ActivityCallback {

    void showLoadingIndicator(boolean active);
}
