package com.app.articlesviewer.view;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.app.articlesviewer.R;
import com.app.articlesviewer.presenter.SearchPresenter;
import com.app.articlesviewer.view.fragments.ArticlesListFragment;
import com.app.articlesviewer.view.fragments.ArticlesSearchFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Julya on 29.05.2017.
 */

public class MainActivity extends AppCompatActivity implements ActivityCallback, SearchView.OnQueryTextListener{
    private static String TAG = "TAG";

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.toolbar_progress_bar)
    protected ProgressBar progressBar;

    protected SearchView mSearchView;

    private FragmentManager fragmentManager;

    private ArticlesSearchFragment mSearchFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(TAG);
        if(fragment == null) replaceFragment(new ArticlesListFragment(), false);
    }

    private void replaceFragment(Fragment fragment, boolean addBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        if (addBackStack) transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void showLoadingIndicator(boolean active) {
        progressBar.setVisibility(active ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.search);
        mSearchView = (SearchView) searchMenuItem.getActionView();
        mSearchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = fragmentManager.findFragmentByTag(TAG);
        if (fragment instanceof ArticlesSearchFragment) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ArticlesListFragment articlesListFragment = new ArticlesListFragment();
            replaceFragment(articlesListFragment, false);
        }else if (fragment instanceof ArticlesListFragment){
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.search:
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mSearchFragment = new ArticlesSearchFragment();
                replaceFragment(mSearchFragment, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String text) {
        if (text.length() > 0) {
            ((SearchPresenter)mSearchFragment.getPresenter()).searchArticle(text);
        }
        return true;
    }
}
