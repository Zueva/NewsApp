package com.app.articlesviewer.view.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.app.articlesviewer.presenter.BasePresenter;
import com.app.articlesviewer.view.ActivityCallback;

/**
 * Created by Julya on 29.05.2017.
 */

public abstract class ArticlesFragment extends Fragment implements BaseView {

    protected ActivityCallback activityCallback;

    public abstract BasePresenter getPresenter();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            activityCallback = (ActivityCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + "implement activityCallback");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (getPresenter() != null) {
            getPresenter().stop();
        }
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        activityCallback.showLoadingIndicator(active);
    }
}
