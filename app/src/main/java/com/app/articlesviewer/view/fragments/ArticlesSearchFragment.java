package com.app.articlesviewer.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.articlesviewer.R;
import com.app.articlesviewer.adapters.SearchResultAdapter;
import com.app.articlesviewer.model.Article;
import com.app.articlesviewer.presenter.BasePresenter;
import com.app.articlesviewer.presenter.SearchContract;
import com.app.articlesviewer.presenter.SearchPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.articlesviewer.R.id.recyclerView;

/**
 * Created by Julya on 29.05.2017.
 */

public class ArticlesSearchFragment extends ArticlesFragment implements SearchContract.View {

    private static final String SAVED_LAYOUT_MANAGER = "searchLayoutManager";

    @BindView(recyclerView)
    protected RecyclerView mRecyclerView;

    protected SearchResultAdapter mAdapter;

    private SearchPresenter mSearchPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mSearchPresenter = new SearchPresenter(this);
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_search_articles, container, false);
        ButterKnife.bind(this, view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        if (savedInstanceState != null) {
            int position = (int)savedInstanceState.getSerializable(SAVED_LAYOUT_MANAGER);
            mRecyclerView.getLayoutManager().scrollToPosition(position);
        }

        mAdapter = new SearchResultAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mSearchPresenter.createView();

        return view;
    }

    @Override
    public void showLoadingError(String error) {
        Toast.makeText(getContext(), R.string.error_data, Toast.LENGTH_SHORT).show();
    }

    @Override
    public BasePresenter getPresenter() {
        return mSearchPresenter;
    }

    @Override
    public void showSearchResult(List<Article> articles) {
        mAdapter.setSearchResult(articles);
    }

    @Override
    public void showNoResult() {
        Toast.makeText(getContext(), R.string.no_data, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        int mScrollPosition = 0;
        RecyclerView.LayoutManager layoutManager = mRecyclerView.getLayoutManager();
        if (layoutManager != null && layoutManager instanceof LinearLayoutManager)  {
            mScrollPosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        }
        outState.putSerializable(SAVED_LAYOUT_MANAGER, mScrollPosition);
    }
}
