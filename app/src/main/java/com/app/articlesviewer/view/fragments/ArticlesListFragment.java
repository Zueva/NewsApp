package com.app.articlesviewer.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.articlesviewer.R;
import com.app.articlesviewer.adapters.ArticleAdapter;
import com.app.articlesviewer.model.Article;
import com.app.articlesviewer.presenter.ArticlesContract;
import com.app.articlesviewer.presenter.ArticlesPresenter;
import com.app.articlesviewer.presenter.BasePresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.articlesviewer.R.id.recyclerView;

/**
 * Created by Julya on 29.05.2017.
 */

public class ArticlesListFragment extends ArticlesFragment implements ArticlesContract.View {

    private static final String SAVED_LAYOUT_MANAGER = "articleslLayoutManager";

    @BindView(recyclerView)
    protected RecyclerView mRecyclerView;

    private ArticlesPresenter mArticlePresenter;

    protected ArticleAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mArticlePresenter = new ArticlesPresenter(this);
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    private int offset = 0;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_list_articles, container, false);
        ButterKnife.bind(this, view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        if (savedInstanceState != null) {
            int position = (int)savedInstanceState.getSerializable(SAVED_LAYOUT_MANAGER);
            mRecyclerView.getLayoutManager().scrollToPosition(position);
        }

        mAdapter = new ArticleAdapter(mRecyclerView);
        mAdapter.setOnLoadMoreListener(() -> mArticlePresenter.start(offset));
        mRecyclerView.setAdapter(mAdapter);
        mArticlePresenter.createView(offset);

        return view;
    }

    @Override
    public void showLoadingError(String error) {
        mAdapter.stopProgress(false);
        makeToast(getString(R.string.error_data));
    }

    @Override
    public void showArticles(List<Article> articles, int offset) {
        articles.add(null);
        mAdapter.setArticles(articles);
        this.offset = offset;
    }

    @Override
    public void showNoArticles() {
        mAdapter.stopProgress(true);
    }

    @Override
    public BasePresenter getPresenter() {
        return mArticlePresenter;
    }

    private void makeToast(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        int mScrollPosition= 0;
        RecyclerView.LayoutManager layoutManager = mRecyclerView.getLayoutManager();
        if (layoutManager != null && layoutManager instanceof LinearLayoutManager)  {
            mScrollPosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        }
            outState.putSerializable(SAVED_LAYOUT_MANAGER, mScrollPosition);
    }
}
