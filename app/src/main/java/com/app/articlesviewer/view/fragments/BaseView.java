package com.app.articlesviewer.view.fragments;

/**
 * Created by Yulya on 27.05.2017.
 */

public interface BaseView {

    void setLoadingIndicator(boolean active);

    void showLoadingError(String error);
}
