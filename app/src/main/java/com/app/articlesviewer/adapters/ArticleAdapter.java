package com.app.articlesviewer.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.articlesviewer.ArticlesViewerApplication;
import com.app.articlesviewer.R;
import com.app.articlesviewer.model.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yulya on 28.05.2017.
 */

public class ArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @Inject
    protected Picasso picasso;

    private final int VIEW_TYPE_ITEM = 0;

    private final int VIEW_TYPE_LOADING = 1;

    private List<Article> mArticles = new ArrayList<>();

    private OnLoadMoreListener onLoadMoreListener;

    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private boolean stopProgress = false;

    public ArticleAdapter(RecyclerView recyclerView) {
        ArticlesViewerApplication.getAppComponent().inject(this);

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && !stopProgress &&  lastVisibleItem == totalItemCount - 1) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setArticles(List<Article> articles) {
        mArticles.removeAll(Collections.singleton(null));
        mArticles.addAll(articles);
        notifyDataSetChanged();
        setLoaded();
    }

    public void stopProgress(boolean finish) {
        mArticles.removeAll(Collections.singleton(null));
        notifyDataSetChanged();
        if (finish) stopProgress = true;
    }
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_article, parent, false);
            return new ViewHolder(v);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.date.setText(mArticles.get(position).getDate());
            viewHolder.title.setText(mArticles.get(position).getTitle());
            viewHolder.description.setText(mArticles.get(position).getDescription());
            String imgUrl = mArticles.get(position).getPicture();
            if (imgUrl != null) {
                picasso.load(imgUrl).placeholder(R.drawable.image_default).into(viewHolder.image);
            } else viewHolder.image.setVisibility(View.GONE);
        } else if (holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mArticles == null ? 0 : mArticles.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArticles.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvDate)
        TextView date;
        @BindView(R.id.tvTitle)
        TextView title;
        @BindView(R.id.ivImg)
        ImageView image;
        @BindView(R.id.tvDescription)
        TextView description;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.pbProgress)
        ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
