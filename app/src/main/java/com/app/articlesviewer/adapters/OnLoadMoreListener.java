package com.app.articlesviewer.adapters;

/**
 * Created by Yulya on 29.05.2017.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
