package com.app.articlesviewer.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.articlesviewer.ArticlesViewerApplication;
import com.app.articlesviewer.R;
import com.app.articlesviewer.model.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Julya on 31.05.2017.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {

    @Inject
    protected Picasso picasso;

    private List<Article> mSearchResult = new ArrayList<>();

    public SearchResultAdapter() {
        ArticlesViewerApplication.getAppComponent().inject(this);
    }

    public void setSearchResult(List<Article> searchResult) {
        mSearchResult = searchResult;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_article, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ViewHolder viewHolder = holder;
        viewHolder.date.setText(mSearchResult.get(position).getDate());
        viewHolder.title.setText(mSearchResult.get(position).getTitle());
        viewHolder.description.setText(mSearchResult.get(position).getDescription());
        String imgUrl = mSearchResult.get(position).getPicture();
        if (imgUrl != null) {
            picasso.load(imgUrl).placeholder(R.drawable.image_default).into(viewHolder.image);
        } else viewHolder.image.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return mSearchResult.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvDate)
        TextView date;
        @BindView(R.id.tvTitle)
        TextView title;
        @BindView(R.id.ivImg)
        ImageView image;
        @BindView(R.id.tvDescription)
        TextView description;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
