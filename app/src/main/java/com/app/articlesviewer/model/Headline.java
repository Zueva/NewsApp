package com.app.articlesviewer.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Yulya on 28.05.2017.
 */

public class Headline implements Serializable{
    @SerializedName("main")
    String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
