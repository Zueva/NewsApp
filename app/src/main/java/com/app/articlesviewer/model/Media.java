package com.app.articlesviewer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Julya on 31.05.2017.
 */

public class Media {
    @SerializedName("media-metadata")
    @Expose
    private List<Image> images = new ArrayList<>();

    public List<Image> getImages() {
        return images;
    }
}
