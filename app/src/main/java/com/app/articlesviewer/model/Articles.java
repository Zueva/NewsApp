package com.app.articlesviewer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yulya on 31.05.2017.
 */

public class Articles {
    @SerializedName(value = "results", alternate={"docs"})
    @Expose
    private List<ArticleImpl> articles = new ArrayList<>();

    public List<Article> getItems() {
        return (List<Article>)(Object)articles;
    }

    public void setArticles(List<ArticleImpl> articles) {
        this.articles = articles;
    }
}
