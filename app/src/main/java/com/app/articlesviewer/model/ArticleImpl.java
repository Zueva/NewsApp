package com.app.articlesviewer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Yulya on 27.05.2017.
 */

public class ArticleImpl implements Article {
    @SerializedName("title")
    String title;

    @SerializedName(value="published_date", alternate = "pub_date")
    String date;

    @SerializedName(value = "abstract")
    String description;

    @SerializedName("headline")
    Headline headline;

    @SerializedName("media")
    @Expose
    private List<Media> mediaItems = new ArrayList<>();

    private List<String> urlItems;
    @Override
    public String getTitle() {
        return title != null ? title : getHeadline().getTitle();
    }

    @Override
    public String getDate() {
        return date != null && date.contains("T") ? date.substring(0, date.indexOf("T")) : date;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getPicture() {
        if (urlItems != null) {
        return urlItems.get(0);
        }
        return null;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    public Headline getHeadline() {
        return headline;
    }

    public ArticleImpl(Media ... media) {
        mediaItems = Arrays.asList(media);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setHeadline(Headline headline) {
        this.headline = headline;
    }

    public void setMediaItems(List<String> urls) {
        this.urlItems = urls;
    }

    public void setMediaItems(String ... urls) {
        this.urlItems = Arrays.asList(urls);
    }
}
