package com.app.articlesviewer.model;

/**
 * Created by Yulya on 27.05.2017.
 */

public interface Article {
    String getTitle();

    String getDate();

    String getDescription();

    String getPicture();

    void setTitle(String title);
}
