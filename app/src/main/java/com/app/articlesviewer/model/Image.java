package com.app.articlesviewer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Julya on 31.05.2017.
 */

public class Image {
    @SerializedName("url")
    String url;

    public String getUrl() {
        return url;
    }
}
