package com.app.articlesviewer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yulya on 28.05.2017.
 */

public class SearchArticles {
    @SerializedName(value = "response")
    Articles articles;

    public Articles getArticles() {
        return articles;
    }
}
