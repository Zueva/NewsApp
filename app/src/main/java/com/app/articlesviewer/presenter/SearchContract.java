package com.app.articlesviewer.presenter;

import com.app.articlesviewer.model.Article;
import com.app.articlesviewer.view.fragments.BaseView;

import java.util.List;

/**
 * Created by Julya on 29.05.2017.
 */

public interface SearchContract {
    interface View extends BaseView {

        void showSearchResult(List<Article> articles);

        void showNoResult();
    }
    interface Presenter extends BasePresenter {

        void searchArticle(String querySearch);

        void createView();
    }
}
