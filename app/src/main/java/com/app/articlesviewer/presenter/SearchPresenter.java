package com.app.articlesviewer.presenter;

import com.app.articlesviewer.ArticlesViewerApplication;
import com.app.articlesviewer.model.Article;
import com.app.articlesviewer.other.Constants;
import com.app.articlesviewer.repository.ArticlesRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by Julya on 29.05.2017.
 */

public class SearchPresenter implements SearchContract.Presenter {

    @Inject
    ArticlesRepository mArticlesRepository;
    @Inject
    @Named(Constants.UI_THREAD)
    Scheduler uiThread;

    @Inject
    @Named(Constants.IO_THREAD)
    Scheduler ioThread;

    private final SearchContract.View mSearchView;

    private CompositeDisposable mCompositeDisposable;

    private List<Article> mResultList;

    public SearchPresenter(SearchContract.View mSearchView) {
        this.mSearchView = mSearchView;
        ArticlesViewerApplication.getAppComponent().inject(this);
    }


    @Override
    public void stop() {
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
    }

    @Override
    public void searchArticle(String query) {
        if (mCompositeDisposable == null || mCompositeDisposable.isDisposed()) {
            mCompositeDisposable = new CompositeDisposable();
        }
        Disposable disposable = mArticlesRepository.searchArticle(query)
                .subscribeOn(ioThread)
                .observeOn(uiThread)
                .subscribeWith(new DisposableObserver<List<Article>>(){
                    @Override
                    public void onNext(List<Article> articles) {
                        if(articles != null && !articles.isEmpty()) {
                            if(mResultList == null) {
                                mResultList = new ArrayList<>();
                            }
                            mResultList.clear();
                            mResultList = articles;
                            mSearchView.showSearchResult(mResultList);
                        } else {
                            mSearchView.showNoResult();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mSearchView.showLoadingError(e.toString());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
        mCompositeDisposable.add(disposable);
    }

    @Override
    public void createView() {
        if ((mResultList != null && !mResultList.isEmpty())) {
            mSearchView.showSearchResult(mResultList);
        }
    }
}
