package com.app.articlesviewer.presenter;

/**
 * Created by Yulya on 27.05.2017.
 */

public interface BasePresenter {

    void stop();
}
