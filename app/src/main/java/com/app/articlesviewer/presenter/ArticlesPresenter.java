package com.app.articlesviewer.presenter;

import com.app.articlesviewer.ArticlesViewerApplication;
import com.app.articlesviewer.model.Article;
import com.app.articlesviewer.other.Constants;
import com.app.articlesviewer.repository.ArticlesRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

import static com.app.articlesviewer.other.Constants.ARTICLES_SECTION;
import static com.app.articlesviewer.other.Constants.ARTICLES_TIME_PERIOD;

/**
 * Created by Yulya on 27.05.2017.
 */

public class ArticlesPresenter implements ArticlesContract.Presenter {

    @Inject
    ArticlesRepository mArticlesRepository;
    @Inject
    @Named(Constants.UI_THREAD)
    Scheduler uiThread;

    @Inject
    @Named(Constants.IO_THREAD)
    Scheduler ioThread;

    private final ArticlesContract.View mArticlesView;

    private List<Article> mArticlesList;

    private CompositeDisposable mCompositeDisposable;

    public ArticlesPresenter(ArticlesContract.View mArticlesView) {
        this.mArticlesView = mArticlesView;
        ArticlesViewerApplication.getAppComponent().inject(this);
    }

    @Override
    public void start(int offset) {
        if (mCompositeDisposable == null || mCompositeDisposable.isDisposed()) {
            mCompositeDisposable = new CompositeDisposable();
        }
        if (offset == 0) mArticlesView.setLoadingIndicator(true);

        Disposable disposable = (Disposable) mArticlesRepository.getArticles(ARTICLES_SECTION, ARTICLES_TIME_PERIOD, offset)
                .subscribeOn(ioThread)
                .observeOn(uiThread)
                .subscribeWith(new DisposableObserver<List<Article>>() {
                    @Override
                    public void onNext(List<Article> articles) {
                        if(articles != null && !articles.isEmpty()) {
                            if(mArticlesList == null) {
                                mArticlesList = new ArrayList<Article>();
                            }
                            mArticlesList.clear();
                            mArticlesList = articles;
                            mArticlesRepository.saveArticles(mArticlesList);
                            mArticlesView.showArticles(mArticlesList, offset + Constants.OFFSET_STEP);
                        } else {
                            mArticlesView.showNoArticles();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mArticlesView.setLoadingIndicator(false);
                        mArticlesView.showLoadingError(e.toString());
                    }

                    @Override
                    public void onComplete() {

                        mArticlesView.setLoadingIndicator(false);
                    }
                });

        mCompositeDisposable.add(disposable);
    }

    @Override
    public void stop() {
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
    }

    @Override
    public void createView(int offset) {
        if (mArticlesRepository.cachedArticlesAvailable()){
            mArticlesView.showArticles(new ArrayList<>(mArticlesRepository.getCachedArticles()), offset);
        }
        else {
            start(offset);
        }
    }
}
