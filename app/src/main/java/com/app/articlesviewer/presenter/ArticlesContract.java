package com.app.articlesviewer.presenter;

import com.app.articlesviewer.model.Article;
import com.app.articlesviewer.view.fragments.BaseView;

import java.util.List;

/**
 * Created by Yulya on 27.05.2017.
 */

public interface ArticlesContract {

    interface View extends BaseView {

        void showArticles(List<Article> articles, int offset);

        void showNoArticles();
    }
    interface Presenter extends BasePresenter {

        void start(int offset);

        void createView(int offset);
    }
}
