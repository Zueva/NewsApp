package com.app.articlesviewer;

import com.app.articlesviewer.di.TestComponent;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

/**
 * Created by Julya on 01.06.2017.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class,
        constants = BuildConfig.class
)
public class BaseTest {
    public TestComponent component;
    @Before
    public void setUp() throws Exception {
        component = (TestComponent) ArticlesViewerApplication.getAppComponent();
    }
}
