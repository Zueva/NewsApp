package com.app.articlesviewer;

import com.app.articlesviewer.model.Articles;
import com.app.articlesviewer.presenter.SearchContract;
import com.app.articlesviewer.presenter.SearchPresenter;
import com.app.articlesviewer.repository.ArticlesRepository;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import javax.inject.Inject;

import io.reactivex.Observable;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by Yulya on 01.06.2017.
 */

public class SearchPresenterTest extends BaseTest {
    @Inject
    Articles articles;
    @Inject
    ArticlesRepository articlesRepository;

    private SearchPresenter searchPresenter;
    private SearchContract.View mockView;
    String querySearch;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        component.inject(this);
        mockView = mock(SearchContract.View.class);
        searchPresenter = new SearchPresenter(mockView);
        querySearch = "meseum";
    }

    @Test
    public void searchArticles() {
        doAnswer(invocation -> Observable.fromArray(articles.getItems()))
                .when(articlesRepository)
                .searchArticle(querySearch);

        searchPresenter.searchArticle(querySearch);
        searchPresenter.stop();

        verify(mockView).showSearchResult(articles.getItems());
    }

    @Test
    public void loadEmptyData() {
        doAnswer(invocation -> Observable.just(Collections.emptyList()))
                .when(articlesRepository)
                .searchArticle(querySearch);

        searchPresenter.searchArticle(querySearch);
        verify(mockView).showNoResult();
    }

    @Test
    public void testOnError() {
        doAnswer(invocation -> Observable.error(new Throwable(TestConst.TEST_ERROR)))
                .when(articlesRepository)
                .searchArticle(querySearch);

        searchPresenter.searchArticle(querySearch);
        verify(mockView).showLoadingError(new Throwable(TestConst.TEST_ERROR).toString());
    }
}
