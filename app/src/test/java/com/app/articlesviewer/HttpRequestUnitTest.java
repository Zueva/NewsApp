package com.app.articlesviewer;

import com.app.articlesviewer.model.Articles;
import com.app.articlesviewer.model.SearchArticles;
import com.app.articlesviewer.net.ApiInterface;
import com.app.articlesviewer.other.Constants;

import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import io.reactivex.observers.TestObserver;

/**
 * Created by Julya on 01.06.2017.
 */

public class HttpRequestUnitTest extends BaseTest {
    @Inject
    ApiInterface apiInterface;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        component.inject(this);
    }

    @Test
    public void testGetArticles() {
        String section_arts = "Arts";
        String section_books = "Books";
        int week = 7;
        int day = 1;
        int offset = 20;

        TestObserver<Articles> testObserver = apiInterface.getArticles(section_books, day, Constants.ARTICLES_MOST_POPULAR, offset).test();
        testObserver.assertNoErrors().assertValue(articles -> articles.getItems().size() > 0);
    }

    @Test
    public void testSearchArticles() {
        String searchQuery = "new";
        TestObserver<SearchArticles> testObserver = apiInterface.searchArticle(Constants.ARTICLES_SEARCH, searchQuery).test();
        testObserver.assertNoErrors().assertValue(searchArticles -> searchArticles.getArticles().getItems().size() > 0);
    }
}
