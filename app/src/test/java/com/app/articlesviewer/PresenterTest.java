package com.app.articlesviewer;

import com.app.articlesviewer.model.Articles;
import com.app.articlesviewer.other.Constants;
import com.app.articlesviewer.presenter.ArticlesContract;
import com.app.articlesviewer.presenter.ArticlesPresenter;
import com.app.articlesviewer.repository.ArticlesRepository;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import javax.inject.Inject;

import io.reactivex.Observable;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by Julya on 01.06.2017.
 */

public class PresenterTest extends BaseTest {
    @Inject
    Articles articles;
    @Inject
    ArticlesRepository articlesRepository;

    private ArticlesPresenter articlesPresenter;
    private ArticlesContract.View mockView;
    String sections;
    int time;
    int offset;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        component.inject(this);
        mockView = mock(ArticlesContract.View.class);
        articlesPresenter = new ArticlesPresenter(mockView);
        sections = "Arts";
        time = 7;
        offset = 20;
    }

    @Test
    public void loadArticles() {
        doAnswer(invocation -> Observable.fromArray(articles.getItems()))
                .when(articlesRepository)
                .getArticles(sections, time, offset);

        articlesPresenter.createView(offset);
        articlesPresenter.stop();

        verify(mockView).showArticles(articles.getItems(), offset + Constants.OFFSET_STEP);
    }

    @Test
    public void loadEmptyData() {
        doAnswer(invocation -> Observable.just(Collections.emptyList()))
                .when(articlesRepository)
                .getArticles(sections, time, offset);

        articlesPresenter.createView(offset);
        verify(mockView).showNoArticles();
    }

    @Test
    public void testOnError() {
        doAnswer(invocation -> Observable.error(new Throwable(TestConst.TEST_ERROR)))
                .when(articlesRepository)
                .getArticles(sections, time, offset);

        articlesPresenter.createView(offset);
        verify(mockView).showLoadingError(new Throwable(TestConst.TEST_ERROR).toString());
    }
}
