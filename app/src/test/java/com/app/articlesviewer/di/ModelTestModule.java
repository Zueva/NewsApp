package com.app.articlesviewer.di;

import com.app.articlesviewer.net.ApiInterface;
import com.app.articlesviewer.net.ApiModule;
import com.app.articlesviewer.other.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Julya on 01.06.2017.
 */
@Module
public class ModelTestModule {
    @Provides
    @Singleton
    ApiInterface provideApiInterface() {
        return ApiModule.getApiInterface(Constants.BASE_URL);
    }
}
