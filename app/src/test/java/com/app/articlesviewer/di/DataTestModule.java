package com.app.articlesviewer.di;

import com.app.articlesviewer.TestUtils;
import com.app.articlesviewer.model.Articles;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Julya on 01.06.2017.
 */
@Module
public class DataTestModule {

    private TestUtils testUtils;

    public DataTestModule() {
        testUtils = new TestUtils();
    }

    @Provides
    Articles provideListArticles() {
        return testUtils.getGson().fromJson(testUtils.readString("json/articles_list.json"), Articles.class);
    }
}
