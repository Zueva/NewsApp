package com.app.articlesviewer.di;

import com.app.articlesviewer.other.Constants;
import com.app.articlesviewer.repository.ArticlesRepository;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static org.mockito.Mockito.mock;

/**
 * Created by Julya on 01.06.2017.
 */
@Module
public class PresenterTestModule {
    @Provides
    @Singleton
    ArticlesRepository provideDataRepository() {
        return mock(ArticlesRepository.class);
    }

    @Provides
    @Singleton
    @Named(Constants.UI_THREAD)
    Scheduler provideSchedulerUI() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    @Named(Constants.IO_THREAD)
    Scheduler provideSchedulerIO() {
        return AndroidSchedulers.mainThread();
    }

}
