package com.app.articlesviewer.di;

import android.app.Application;
import android.content.Context;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Julya on 01.06.2017.
 */
@Module
public class AppTestModule {
    private Application mApplication;

    public AppTestModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Context provideApplication() {

        return this.mApplication;
    }

    @Provides
    @Singleton
    Picasso providePicasso() {
        return Picasso.with(mApplication.getApplicationContext());
    }
}
