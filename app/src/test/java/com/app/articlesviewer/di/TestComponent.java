package com.app.articlesviewer.di;

/**
 * Created by Julya on 01.06.2017.
 */

import com.app.articlesviewer.HttpRequestUnitTest;
import com.app.articlesviewer.PresenterTest;
import com.app.articlesviewer.SearchPresenterTest;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ModelTestModule.class, AppTestModule.class, DataTestModule.class, PresenterTestModule.class})
public interface TestComponent extends AppComponent{

    void inject(HttpRequestUnitTest httpRequestUnitTest);

    void inject(PresenterTest presenterTest);

    void inject(SearchPresenterTest searchPresenter);
}
