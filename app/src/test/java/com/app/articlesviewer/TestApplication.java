package com.app.articlesviewer;

import com.app.articlesviewer.di.AppComponent;
import com.app.articlesviewer.di.AppTestModule;
import com.app.articlesviewer.di.DaggerTestComponent;

/**
 * Created by Julya on 01.06.2017.
 */

public class TestApplication extends ArticlesViewerApplication{

    @Override
    protected AppComponent buildAppComponent() {
        return DaggerTestComponent.builder()
                .appTestModule(new AppTestModule(this))
                .build();
    }
}
